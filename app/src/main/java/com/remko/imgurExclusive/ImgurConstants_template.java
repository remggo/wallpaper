package com.remko.imgurExclusive;

/**
 * Created by remko on 12.02.15.
 */
public class ImgurConstants_template {
    public static final String MY_IMGUR_CLIENT_ID = ""; //YOUR Client ID
    public static final String MY_IMGUR_CLIENT_SECRET = ""; // YOUR Secret
    //The arbitrary redirect url (Authorization callback URL), ex. awesome://imgur or http://android,
    // declared when registering your app with Imgur API
    public static final String MY_IMGUR_REDIRECT_URL = "";
}
