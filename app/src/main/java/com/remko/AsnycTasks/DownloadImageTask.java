package com.remko.AsnycTasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.remko.ImageProvider.ImageProvider;
import com.remko.handler.ImageDownloadComplete;

import java.io.InputStream;

/**
 * ASYNC DownloadTask
 */
public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

    private ImageDownloadComplete delegate;
    private ImageProvider imageProvider;
    private ProgressBar mProgressBar;

    public DownloadImageTask(ProgressBar progressBar,
                             ImageProvider imageProvider,
                             ImageDownloadComplete delegate) {
        this.delegate = delegate;
        this.imageProvider = imageProvider;
        this.mProgressBar = progressBar;
    }

    @Override
    protected void onPreExecute() {
        mProgressBar.setIndeterminate(true);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = imageProvider.getUrl();
        Bitmap mIcon11 = null;
        try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
            in.close();
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        delegate.onImageDownloadComplete(result);
    }
}