package com.remko.AsnycTasks;

import android.os.AsyncTask;

import com.remko.handler.OnImageProviderReady;

/**
 * Created by remko on 08.02.15.
 */
public class DummyTask extends AsyncTask<Void, Void, Void> {


    private OnImageProviderReady onImageProviderReady;

    public DummyTask(OnImageProviderReady onImageProviderReady){
        this.onImageProviderReady = onImageProviderReady;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        this.onImageProviderReady.onImageProviderReady();
    }
}
