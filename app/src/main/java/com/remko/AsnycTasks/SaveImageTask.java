package com.remko.AsnycTasks;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by remko on 04.02.15.
 */
public class SaveImageTask extends AsyncTask<String, Void, Void> {

    private Bitmap bmp;

    public SaveImageTask(Bitmap bmp) {
        this.bmp = bmp;
    }

    @Override
    protected Void doInBackground(String... filenames) {
        String filename = filenames[0];
        File file = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                filename);
        if (file.exists() == false) {
            try {
                file.createNewFile();
            } catch (IOException e) {
            }
        }

        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file, false);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
