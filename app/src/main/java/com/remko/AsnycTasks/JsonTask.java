package com.remko.AsnycTasks;

import android.os.AsyncTask;
import android.widget.ProgressBar;

import com.remko.handler.JsonDownloadCallback;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;


/**
 * ASYNC DownloadTask
 */
// usually, subclasses of AsyncTask are declared inside the activity class.
// that way, you can easily modify the UI thread from here
public class JsonTask extends AsyncTask<String, Integer, String> {


    private JsonDownloadCallback taskDelegate;

    public JsonTask(JsonDownloadCallback taskDelegate) {
        this.taskDelegate = taskDelegate;
    }

    @Override
    protected String doInBackground(String... sUrl) {
        try {
            URL website = new URL(sUrl[0]);
            URLConnection connection = website.openConnection();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            connection.getInputStream()));

            StringBuilder response = new StringBuilder();
            String inputLine;

            while ((inputLine = in.readLine()) != null)
                response.append(inputLine);

            in.close();

            return response.toString();
            //catch some possible errors...
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        taskDelegate.onJsonDownloadComplete(result);
    }
}
