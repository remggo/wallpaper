package com.remko.AsnycTasks;

import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.lang.ref.WeakReference;

/**
 * Created by remko on 04.02.15.
 */
public class SetWallpaperTask extends AsyncTask<Void, Void, Void> {

    private WeakReference<ImageView> imageViewWeakReference;
    private Context context;

    public SetWallpaperTask(ImageView imageView, Context context) {
        this.imageViewWeakReference = new WeakReference<ImageView>(imageView);
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        Bitmap bmp = ((BitmapDrawable) imageViewWeakReference.get().getDrawable()).getBitmap();
        WallpaperManager myWallpaperManager
                = WallpaperManager.getInstance(context.getApplicationContext());
        try {
            myWallpaperManager.setBitmap(bmp);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
}
