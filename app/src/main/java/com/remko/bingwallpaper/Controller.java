package com.remko.bingwallpaper;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.remko.AsnycTasks.SaveImageTask;
import com.remko.AsnycTasks.SetWallpaperTask;
import com.remko.ImageProvider.Bing;
import com.remko.ImageProvider.ImageProvider;
import com.remko.handler.ImageDownloadComplete;
import com.remko.handler.OnImageProviderReady;

/**
 * Created by remko on 03.02.15.
 */
public class Controller implements ImageDownloadComplete, OnImageProviderReady, Palette.PaletteAsyncListener {

    private Toolbar toolbar;
    private Context context;
    private ProgressBar progressBar;
    private ImageView imageView;
    private RelativeLayout buttonBar;
    private Window window;

    private ImageProvider imageProvider;
    private Proxy proxy;

    /**
     * API: http://www.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1&mkt=en-US
     * format: js (JSON) | xml | rss
     * idx: StartIndex (0 = heute, 1 = gestern, etc.)
     * n: Anzahl der Einträge
     * mkt: (OPTIONAL) Localization: en-US | de-DE etc..
     */

    /**
     * PUBLIC
     */

    public Controller(Context context,
                      ProgressBar progressBar,
                      ImageView imageView,
                      RelativeLayout buttonBar,
                      Toolbar toolbar,
                      Window window,
                      String provider,
                      int startIndex) {
        this.context = context;
        this.progressBar = progressBar;
        this.imageView = imageView;
        this.buttonBar = buttonBar;
        this.toolbar = toolbar;
        this.window = window;

        // Set Current ImageProvider
        switch (provider) {
            case "Bing":
                imageProvider = new Bing(this, startIndex);
                break;
            default:
                imageProvider = new Bing(this, 0);
                break;
        }
        this.proxy = new Proxy(imageView, progressBar);
    }


    public boolean nextImage() {
        boolean result = imageProvider.nextPhoto();
        loadImage();
        return result;
    }

    public boolean previousImage() {
        boolean result = imageProvider.previousPhoto();
        loadImage();
        return result;
    }

    public void setWallpaper() {
        SetWallpaperTask setWallpaperTask = new SetWallpaperTask(imageView, this.context);
        setWallpaperTask.execute();
    }

    public void loadImage() {
        proxy.setImage(imageProvider, this, this);
    }


    @Override
    public void onImageDownloadComplete(Bitmap result) {
        progressBar.setVisibility(View.GONE);
        imageView.setImageBitmap(result);

        SaveImageTask saveImageTask = new SaveImageTask(result);
        saveImageTask.execute(imageProvider.getFilename());

        Palette.generateAsync(result, this);

    }
    // 0xff3f51b5 = primary Blue, war sonst 50% opaque
    // 0xff303f9f = primary Dark blue, war sonst 50% opaque

    @Override
    public void onGenerated(Palette palette) {
        buttonBar.setBackgroundColor(palette.getVibrantColor(0xff3f51b5));
        toolbar.setBackgroundColor(palette.getVibrantColor(0xff3f51b5));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(palette.getDarkVibrantColor(0xff303f9f));
        }
    }

    @Override
    public void onImageProviderReady() {
        loadImage();
    }

    public int getIndex() {
        return imageProvider.getIndex();
    }

    public String getImageProviderString() {
        return imageProvider.toString();
    }
}
