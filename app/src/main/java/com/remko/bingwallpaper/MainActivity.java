package com.remko.bingwallpaper;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;


public class MainActivity extends ActionBarActivity {

    private static final String IMAGE_PROVIDER = "image_provider";
    private static final String INDEX = "index";
    private static final String NEXT_BUTTON_ENABLED = "next_button_enabled";
    private Controller controller;

    private static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .build();

        MainActivity.context = getApplicationContext();

        ImageLoader.getInstance().init(config);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_awesome_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setElevation(5f);

        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        ImageView imageView = (ImageView) findViewById(R.id.currentImage);
        RelativeLayout buttonBar = (RelativeLayout) findViewById(R.id.buttonBar);

        Window window = getWindow();

        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        String provider = sharedPreferences.getString(IMAGE_PROVIDER, "Bing");
        int index = sharedPreferences.getInt(INDEX, 0);

        boolean enabled = sharedPreferences.getBoolean(NEXT_BUTTON_ENABLED, false);
        findViewById(R.id.nextImage).setEnabled(enabled);
        controller = new Controller(this, progressBar, imageView, buttonBar, toolbar, window, provider, index);
    }

    @Override
    protected void onStop() {

        boolean nextImage_state = findViewById(R.id.nextImage).isEnabled();

        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(INDEX, controller.getIndex());
        editor.putString(IMAGE_PROVIDER, controller.getImageProviderString());
        editor.putBoolean(NEXT_BUTTON_ENABLED, nextImage_state);
        editor.apply();
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        return super.onOptionsItemSelected(item);
    }

    public void setAsWallpaper(View view) {
        this.controller.setWallpaper();
    }

    public void nextImage(View view) {
        boolean result = this.controller.nextImage();
        findViewById(R.id.nextImage).setEnabled(result);
    }

    public void previousImage(View view) {
        findViewById(R.id.nextImage).setEnabled(true);
        this.controller.previousImage();
    }

    public static Context getAppContext() {
        return context;
    }

}
