package com.remko.bingwallpaper;

import android.os.Environment;
import android.support.v7.graphics.Palette;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.remko.AsnycTasks.BitmapWorkerTask;
import com.remko.AsnycTasks.DownloadImageTask;
import com.remko.ImageProvider.ImageProvider;
import com.remko.handler.ImageDownloadComplete;

import java.io.File;

/**
 * Created by remko on 06.02.15.
 */
public class Proxy {

    private String fileBase = null;
    private ImageView imageView;
    private ProgressBar progressBar;

    public Proxy(ImageView imageView, ProgressBar progressBar) {
        fileBase = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath();
        this.imageView = imageView;
        this.progressBar = progressBar;
    }

    public void setImage(ImageProvider imageProvider, ImageDownloadComplete imageDelegate, Palette.PaletteAsyncListener listener) {

        File image = new File(fileBase, imageProvider.getFilename());
        float size = image.length() / 1048576f;
        if (size > 0.5f && !image.isDirectory() && image.exists()) {
            BitmapWorkerTask bitmapWorkerTask = new BitmapWorkerTask(imageView, listener, progressBar);
            bitmapWorkerTask.execute(image.getAbsolutePath());

        } else {
            DownloadImageTask downloadImageTask = new DownloadImageTask(progressBar, imageProvider, imageDelegate);
            downloadImageTask.execute();
        }
    }
}
