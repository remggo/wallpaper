package com.remko.handler;

import android.graphics.Bitmap;

/**
 * Created by remko on 08.02.15.
 */
public interface ImageDownloadComplete {
    public void onImageDownloadComplete(Bitmap result);
}
