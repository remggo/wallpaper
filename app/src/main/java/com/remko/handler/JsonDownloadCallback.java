package com.remko.handler;

public interface JsonDownloadCallback {
    public void onJsonDownloadComplete(String result);
}