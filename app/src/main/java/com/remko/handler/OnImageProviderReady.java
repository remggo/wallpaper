package com.remko.handler;

/**
 * Created by remko on 08.02.15.
 */
public interface OnImageProviderReady {
    public void onImageProviderReady();
}
