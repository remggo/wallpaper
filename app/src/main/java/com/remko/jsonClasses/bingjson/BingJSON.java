package com.remko.jsonClasses.bingjson;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class BingJSON {

    @Expose
    private List<Image> images = new ArrayList<Image>();
    @Expose
    private Tooltips tooltips;

    /**
     * @return The images
     */
    public List<Image> getImages() {
        return images;
    }

    /**
     * @param images The images
     */
    public void setImages(List<Image> images) {
        this.images = images;
    }

    /**
     * @return The tooltips
     */
    public Tooltips getTooltips() {
        return tooltips;
    }

    /**
     * @param tooltips The tooltips
     */
    public void setTooltips(Tooltips tooltips) {
        this.tooltips = tooltips;
    }

}
