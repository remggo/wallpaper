package com.remko.jsonClasses.bingjson;

import com.google.gson.annotations.Expose;

public class H {

    @Expose
    private String desc;
    @Expose
    private String link;
    @Expose
    private String query;
    @Expose
    private int locx;
    @Expose
    private int locy;

    /**
     * @return The desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc The desc
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * @return The link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param link The link
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * @return The query
     */
    public String getQuery() {
        return query;
    }

    /**
     * @param query The query
     */
    public void setQuery(String query) {
        this.query = query;
    }

    /**
     * @return The locx
     */
    public int getLocx() {
        return locx;
    }

    /**
     * @param locx The locx
     */
    public void setLocx(int locx) {
        this.locx = locx;
    }

    /**
     * @return The locy
     */
    public int getLocy() {
        return locy;
    }

    /**
     * @param locy The locy
     */
    public void setLocy(int locy) {
        this.locy = locy;
    }

}
