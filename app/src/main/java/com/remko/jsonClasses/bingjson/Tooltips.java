package com.remko.jsonClasses.bingjson;

import com.google.gson.annotations.Expose;

public class Tooltips {

    @Expose
    private String loading;
    @Expose
    private String previous;
    @Expose
    private String next;
    @Expose
    private String walle;
    @Expose
    private String walls;

    /**
     * @return The loading
     */
    public String getLoading() {
        return loading;
    }

    /**
     * @param loading The loading
     */
    public void setLoading(String loading) {
        this.loading = loading;
    }

    /**
     * @return The previous
     */
    public String getPrevious() {
        return previous;
    }

    /**
     * @param previous The previous
     */
    public void setPrevious(String previous) {
        this.previous = previous;
    }

    /**
     * @return The next
     */
    public String getNext() {
        return next;
    }

    /**
     * @param next The next
     */
    public void setNext(String next) {
        this.next = next;
    }

    /**
     * @return The walle
     */
    public String getWalle() {
        return walle;
    }

    /**
     * @param walle The walle
     */
    public void setWalle(String walle) {
        this.walle = walle;
    }

    /**
     * @return The walls
     */
    public String getWalls() {
        return walls;
    }

    /**
     * @param walls The walls
     */
    public void setWalls(String walls) {
        this.walls = walls;
    }

}
