package com.remko.jsonClasses.bingjson;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class Image {

    @Expose
    private String startdate;
    @Expose
    private String fullstartdate;
    @Expose
    private String enddate;
    @Expose
    private String url;
    @Expose
    private String urlbase;
    @Expose
    private String copyright;
    @Expose
    private String copyrightlink;
    @Expose
    private boolean wp;
    @Expose
    private String hsh;
    @Expose
    private int drk;
    @Expose
    private int top;
    @Expose
    private int bot;
    @Expose
    private List<H> hs = new ArrayList<H>();
    @Expose
    private List<Object> msg = new ArrayList<Object>();

    /**
     * @return The startdate
     */
    public String getStartdate() {
        return startdate;
    }

    /**
     * @param startdate The startdate
     */
    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    /**
     * @return The fullstartdate
     */
    public String getFullstartdate() {
        return fullstartdate;
    }

    /**
     * @param fullstartdate The fullstartdate
     */
    public void setFullstartdate(String fullstartdate) {
        this.fullstartdate = fullstartdate;
    }

    /**
     * @return The enddate
     */
    public String getEnddate() {
        return enddate;
    }

    /**
     * @param enddate The enddate
     */
    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    /**
     * @return The url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return The urlbase
     */
    public String getUrlbase() {
        return urlbase;
    }

    /**
     * @param urlbase The urlbase
     */
    public void setUrlbase(String urlbase) {
        this.urlbase = urlbase;
    }

    /**
     * @return The copyright
     */
    public String getCopyright() {
        return copyright;
    }

    /**
     * @param copyright The copyright
     */
    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    /**
     * @return The copyrightlink
     */
    public String getCopyrightlink() {
        return copyrightlink;
    }

    /**
     * @param copyrightlink The copyrightlink
     */
    public void setCopyrightlink(String copyrightlink) {
        this.copyrightlink = copyrightlink;
    }

    /**
     * @return The wp
     */
    public boolean isWp() {
        return wp;
    }

    /**
     * @param wp The wp
     */
    public void setWp(boolean wp) {
        this.wp = wp;
    }

    /**
     * @return The hsh
     */
    public String getHsh() {
        return hsh;
    }

    /**
     * @param hsh The hsh
     */
    public void setHsh(String hsh) {
        this.hsh = hsh;
    }

    /**
     * @return The drk
     */
    public int getDrk() {
        return drk;
    }

    /**
     * @param drk The drk
     */
    public void setDrk(int drk) {
        this.drk = drk;
    }

    /**
     * @return The top
     */
    public int getTop() {
        return top;
    }

    /**
     * @param top The top
     */
    public void setTop(int top) {
        this.top = top;
    }

    /**
     * @return The bot
     */
    public int getBot() {
        return bot;
    }

    /**
     * @param bot The bot
     */
    public void setBot(int bot) {
        this.bot = bot;
    }

    /**
     * @return The hs
     */
    public List<H> getHs() {
        return hs;
    }

    /**
     * @param hs The hs
     */
    public void setHs(List<H> hs) {
        this.hs = hs;
    }

    /**
     * @return The msg
     */
    public List<Object> getMsg() {
        return msg;
    }

    /**
     * @param msg The msg
     */
    public void setMsg(List<Object> msg) {
        this.msg = msg;
    }

}
