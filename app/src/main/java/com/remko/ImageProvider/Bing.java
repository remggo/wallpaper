package com.remko.ImageProvider;

import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.remko.AsnycTasks.JsonTask;
import com.remko.handler.JsonDownloadCallback;
import com.remko.handler.OnImageProviderReady;
import com.remko.jsonClasses.bingjson.BingJSON;

/**
 * Created by remko on 06.02.15.
 */
public class Bing extends ImageProvider implements JsonDownloadCallback {

    private static final String urlBase = "http://www.bing.com";
    private static final String apiUrl = urlBase + "/HPImageArchive.aspx?format=js";
    /**
     * MEMBER
     */
    private BingJSON bingJSON;
    private int currentIndex;
    private OnImageProviderReady onImageProviderReady;
    private ProgressBar progressBar;
    private boolean isCallbackNeeded;

    /**
     * PUBLIC
     */
    public Bing(OnImageProviderReady onImageProviderReady, int startIndex) {
        currentIndex = startIndex;
        updateJson();
        isCallbackNeeded = true;
        this.onImageProviderReady = onImageProviderReady;
    }

    @Override
    public String getUrl() {
        String url = null;
        if (bingJSON != null) {
            url = urlBase + bingJSON.getImages()
                    .get(0).getUrl();
        }
        return url;
    }

    @Override
    public String getFilename() {
        String filename = "Bing_";
        if (bingJSON != null) {
            filename += bingJSON.getImages()
                    .get(0).getStartdate() + ".png";
        }
        return filename;
    }

    @Override
    public String getDescription() {
        return "StaticDescription";
    }

    @Override
    public boolean nextPhoto() {
        boolean result;
        if (currentIndex > 0) {
            currentIndex--;
        }

        if (currentIndex == 0) {
            result = false;
        } else {
            result = true;
        }

        updateJson();
        return result;
    }


    @Override
    public boolean previousPhoto() {
        currentIndex++;
        updateJson();
        return true;
    }

    @Override
    public String toString() {
        return "Bing";
    }

    @Override
    public int getIndex() {
        return currentIndex;
    }

    @Override
    public void onJsonDownloadComplete(String result) {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        bingJSON = gson.fromJson(result, BingJSON.class);
        onImageProviderReady.onImageProviderReady();
    }

    /**
     * PRIVATE
     */

    private void updateJson() {
        JsonTask asyncJson = new JsonTask(this);
        String apiCall = apiUrl + "&idx=" + currentIndex + "&n=1";
        asyncJson.execute(apiCall);
    }


}
