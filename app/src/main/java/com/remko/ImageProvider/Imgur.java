package com.remko.ImageProvider;

import com.remko.AsnycTasks.DummyTask;
import com.remko.AsnycTasks.JsonTask;
import com.remko.Constants;
import com.remko.handler.JsonDownloadCallback;
import com.remko.handler.OnImageProviderReady;
import com.remko.jsonClasses.imgurjson.ImgurJson;

/**
 * Created by remko on 12.02.15.
 */
public class Imgur extends ImageProvider implements JsonDownloadCallback {

    /**
     * How-To: Get Subreddit Gallery!
     * GET https://api.imgur.com/3/gallery/r/{subreddit}/{sort}/{page}
     * {subreddit}: valid Subreddit name (requiered)
     * {sort}: time|top - defaults to time (optional)
     * {page}: integer - the data paging number (optional)
     *
     * URL: https://i.imgur.com/{id}.jpg
     */

    private static final String apiJsonUrl = "https://api.imgur.com/3/gallery/r/";


    private String mSubreddit;
    private ImgurJson imgurJson;
    private OnImageProviderReady onImageProviderReady;


    public Imgur(OnImageProviderReady onImageProviderReady) {
        this(Constants.DEFAULT_SUBREDDIT, onImageProviderReady);
    }

    public Imgur(String subredditname, OnImageProviderReady onImageProviderReady) {
        mSubreddit = subredditname;
        this.onImageProviderReady = onImageProviderReady;
        // Get current JSON galleryList
        JsonTask jsonTask = new JsonTask(this);
        jsonTask.execute();
    }

    public void setSubreddit(String subredditname) {
        mSubreddit = subredditname;
    }

    @Override
    public String getUrl() {
        return null;
    }

    @Override
    public String getFilename() {
        return null;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public boolean nextPhoto() {
        return false;
    }

    @Override
    public boolean previousPhoto() {
        return false;
    }

    @Override
    public String toString() {
        return null;
    }

    @Override
    public int getIndex() {
        return 0;
    }

    @Override
    public void onJsonDownloadComplete(String result) {
        this.onImageProviderReady.onImageProviderReady();
    }

    private String getJsonUrl() {
        return apiJsonUrl + "/" + mSubreddit; // sort und page nicht gebraucht (vorerst)
    }

}
