package com.remko.ImageProvider;

/**
 * Created by remko on 06.02.15.
 */
public abstract class ImageProvider {
    public abstract String getUrl();

    public abstract String getFilename();

    public abstract String getDescription();

    public abstract boolean nextPhoto();

    public abstract boolean previousPhoto();

    public abstract String toString();

    public abstract int getIndex();
}
